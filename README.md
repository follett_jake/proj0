# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

## Instructions:
---------------
- Author: Jake Follett

- Contact address: jfollett@uoregon.edu

- Description: This project is exercise in using git, config tool, checker. when you   write make run in the terminal this will print "Hello world". In hello.py we import  configparser to  read credentials.ini file. From this file we can access the         message by
  config["DEFAULT"]["message"]. That is how we can get the message "Hello world". 
